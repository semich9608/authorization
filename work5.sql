-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Апр 25 2022 г., 01:14
-- Версия сервера: 10.4.21-MariaDB
-- Версия PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `work5`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `tokens`
--

INSERT INTO `tokens` (`id`, `created_at`, `value`) VALUES
(2, '2022-04-23 17:03:12', '009745f5bb2c5f9167a7b722a4fd27b834697e33'),
(6, '2022-04-24 15:58:15', '9da0a264937a3097410fe719d135f53c62320599'),
(28, '2022-04-24 23:31:57', '5a5d7796b1f50786a298ccf6c285cc7bbff98ade');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(345) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `email`) VALUES
(1, 'root', '$2y$10$g440XRZAebpRQwL0D8YUtO5OqA4xmCiuY3DFzvzMsCbiquYQOT7Nm', 'semich9608@gmail.com'),
(3, 'asda', 'as', 'asdasd'),
(4, 'asdasss', 'aa', 'asdasdsss'),
(5, 'Yana', 'aa', 'ss'),
(6, 'ggg', '$2y$10$12RXMJLF/XgzJ718rRHtGeI3W./9ZD7fn1aLDLSOreyeueOk.VDTy', 'kl9'),
(7, 'some', '$2y$10$LxJykmqYt4c43Z7C4JF1y.uLPO0vbvudKF/a1ybjMpsI7a6Bmwi62', 'some@'),
(8, 'asdas123412431asdAS', '$2y$10$mPdqxjhSPQDd6nuWOmCye.acOqhCqZkIkWgFWCm1TmOneRS8.evHu', 'as'),
(9, 'isafwsaef', '$2y$10$Py.3zYAdVC65Mn.KZzZ7FuTbEboDqKwD7woO2tVAeyEqs1hA8/7dC', 'semich2001@mail.ru'),
(10, 'isafwsaefs', '$2y$10$EP6z9Pl/SoPeTvzZkt4jIebkoKyN17ykViDHNsxhA1mGEJ9ye8xB6', 'semich2001@mail.rus'),
(11, 'someOne', '$2y$10$lKkC.YiCRqcnND1h.naMc.LYFyqaJFl14f7e9YBemX6mUzdxUP/dm', 'someOm@m.ru'),
(12, 'kkwWWwWdDD1123123', 'sdsd', 'wsgmpkagpr@mail.ru'),
(13, 'yana', '$2y$10$n/xGNO9CNqOoIGtGXrC.r.G.6fUI8Z63tfnpc2Mwr82bhWjWDIGvm', 'semich200ss1@mail.rus'),
(14, 'hjkk', '$2y$10$BgT/fY.abtZpvsaFPLnP..e35kjAvA4pI/x7qnHYEeAkqtNtn3r2K', 's@m.r'),
(15, 'kl', '$2y$10$oIs/RLU7YeE1Q14QS5DDtuSza4LOboYxnBs5vvFTnY1N6Gsl6w7vq', 'k@m.r'),
(16, 'kavo', '$2y$10$2E5bjy701foS1Q3NiodgBOULiWKyaNUFAYaNXVtlYerOiPyhAkLmS', 'kavo@mail.ru'),
(17, 'kot', '$2y$10$a1EWwEKrjXyxb0Jcxqc7B.2RpOHq0HuQ2vv6rJixHUzU6o1Zx1C6G', 'setSuccessss@m.rgs'),
(18, 'psyDuck', '$2y$10$0yMOwzXRv/.eqABc0lGAouUoA0KMLurlQNkBu01OmKrtnkvHnZfY2', 'psyduck@top.ru'),
(23, 'ffffffff', '$2y$10$5oTLe8U8i7Kxl1Pl6JbF2uqL6QAaqsxuCdDZQilUW7pCo7mCUHobi', 'ff@m.rt'),
(24, 'registrationSuccessful', '$2y$10$WnCIZC/i3327WyxaYLIVBO2B5nKU/BhrKMDeqwBndpU.mQmp9Qype', 'reg@m.r'),
(25, 'registrationSuccessfulggg', '$2y$10$aqdjZAM//ODuVPb4dNsI3O/OOnaugtTm1t0H3Id18yZm.X.ZFPOvG', 'dfd@m.r');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
