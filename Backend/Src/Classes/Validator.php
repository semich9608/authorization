<?php

namespace App\Classes;

use Exception;
use App\Orm\Repositories\UserRepository;

class Validator
{
    private const REGEX_LOGIN = '/^[A-Za-z0-9]{1,100}$/';
    private const REGEX_PASSWORD = '/^[A-Za-z0-9]{1,50}$/';
    private const REGEX_EMAIL = '/^[A-Za-z0-9]{1,128}+@[A-Za-z0-9]{1,212}+\.[a-z]{1,3}$/';

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws Exception
     */
    static public function authValidate(?string $login, ?string $password)
    {
        if (empty($login) || empty($password)) {
            throw new Exception('Login and password can`t be a null', 400);
        }
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @param string|null $repeat_password
     * @param string|null $email
     * @throws Exception
     */
    static public function registrationValidate(?string $login, ?string $password, ?string $repeat_password, ?string $email)
    {
        if (empty($login) || empty($password) || empty($repeat_password) || empty($email)) {
            throw new Exception('Login, password, repeating password and email can`t be a null', 400);
        }

        if (!preg_match(self::REGEX_LOGIN, $login)) {
            throw new Exception('Login must be more than 0 characters, but no more than 100 characters and consist of Latin letters or numbers.', 400);
        }

        if (!preg_match(self::REGEX_PASSWORD, $password) || !preg_match(self::REGEX_PASSWORD, $repeat_password)) {
            throw new Exception('Password and repeating password must be more than 0 characters, but no more than 50 characters and consist of Latin letters or numbers.', 400);
        }

        if (!preg_match(self::REGEX_EMAIL, $email)) {
            throw new Exception('Email must be lock like email address.', 400);
        }
    }

    /**
     * @throws Exception
     */
    static public function changeEmailValidate(?string $currentEmail, ?string $newEmail)
    {
        if ($currentEmail === $newEmail) {
            throw new Exception('Current email and new email must be a different.', 400);
        }

        if (empty($newEmail)) {
            throw new Exception('Email can`t be a null', 400);
        }

        if (!preg_match(self::REGEX_EMAIL, $newEmail)) {
            throw new Exception('Email must be lock like email address.', 400);
        }
    }

    /**
     * @throws Exception
     */
    static public function changePasswordValidate(?string $currentPassword, ?string $newPassword, ?string $confirmNewPassword)
    {
        if (empty($currentPassword) || empty($newPassword) || empty($confirmNewPassword)) {
            throw new Exception('Passwords can`t be a null', 400);
        }

        if (!preg_match(
                self::REGEX_PASSWORD, $currentPassword)
            || !preg_match(self::REGEX_PASSWORD, $newPassword)
            || !preg_match(self::REGEX_PASSWORD, $confirmNewPassword)) {
            throw new Exception('Passwords must be more than 0 characters, but no more than 50 characters and consist of Latin letters or numbers.', 400);
        }

        if (!password_verify(
            $currentPassword, (new UserRepository())->findBy(['login' => $_SESSION['login']])->getPassword())) {
            throw new Exception('Incorrect current password.', 400);
        }

        if ($currentPassword === $newPassword) {
            throw new Exception('Current and new passwords can`t be the same.');
        }

        if ($newPassword !== $confirmNewPassword) {
            throw new Exception('New password and repeating new passwords must be the same.');
        }
    }
}