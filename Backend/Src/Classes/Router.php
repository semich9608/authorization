<?php

namespace App\Classes;

class Router
{
    private const NOT_FOUND_PAGE_PATH = 'Frontend/Public/Pages/NotFoundPage.html';
    private const PAGES_DIRECTORY = 'Frontend/Public/Pages/';
    private const CONTROLLERS_NAMESPACE = '\App\Controllers\\';

    private array $routes = [
        'get' => [],
        'post' => [],
    ];

    public function __construct()
    {
        $this->createUriPull();
    }

    public function addGetRoute(string $uri, string $pageName)
    {
        $this->routes['get'][$uri] = self::PAGES_DIRECTORY . $pageName;
    }

    public function addPostRoute(string $uri, string $controllerName, string $action)
    {
        $this->routes['post'][$uri] = [
            'path' => self::CONTROLLERS_NAMESPACE . $controllerName,
            'action' => $action,
        ];
    }

    public function methodGet(string $uri)
    {
        if (in_array($uri, $this->getGetRoutes())) {
            require_once $this->routes['get'][$uri];
        } else {
            require_once self::NOT_FOUND_PAGE_PATH;
        }
        exit();
    }

    public function methodPost(string $uri, $data)
    {
        if (in_array($uri, array_keys($this->routes['post']))) {
            $controller = new $this->routes['post'][$uri]['path']() ?? null;
            $action = $this->routes['post'][$uri]['action'] ?? null;
            $controller->$action($data);
        } else {
            require_once self::NOT_FOUND_PAGE_PATH;
        }
    }

    public function getGetRoutes(): array
    {
        return array_keys($this->routes['get']);
    }

    private function createUriPull()
    {
        $this->addGetRoute('', 'Auth.html');
        $this->addGetRoute('/', 'Auth.html');
        $this->addGetRoute('/auth', 'Auth.html');
        $this->addGetRoute('/registration', 'Registration.html');
        $this->addGetRoute('/welcome', 'Welcome.html');

        $this->addPostRoute('/authorization', 'AuthController', 'authAction');
        $this->addPostRoute('/registration', 'AuthController', 'registrationAction');
        $this->addPostRoute('/welcome/get_data', 'WelcomeController', 'getDataAction');
        $this->addPostRoute('/welcome/exit', 'WelcomeController', 'exitAction');
        $this->addPostRoute('/welcome/change_email', 'WelcomeController', 'changeEmailAction');
        $this->addPostRoute('/welcome/change_password', 'WelcomeController', 'changePasswordAction');
    }
}