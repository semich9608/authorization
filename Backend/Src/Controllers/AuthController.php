<?php

namespace App\Controllers;

use App\Classes\Validator;
use Exception;
use App\Orm\Repositories\TokenRepository;
use App\Orm\Repositories\UserRepository;

class AuthController extends Controller
{
    private const MESSAGES = [
        'login_error' => 'Incorrect login or password.',
        'email_exists' => 'User with this email is already exists.',
        'login_exists' => 'User with this login is already exists',
        'different_passwords' => 'Passwords must be the same',
        'registration_successful' => 'Registration successful, you will be taken to the authorization page.'
    ];

    private UserRepository $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    /**
     * @param array $data
     * @return bool|int
     */
    public function authAction(array $data): bool|int
    {
        $tokenRepository = new TokenRepository();

        $login = $data['login'] ?? null;
        $password = $data['password'] ?? null;

        try {
            Validator::authValidate($login, $password);
        } catch (Exception $e) {
            return $this->returnResponse($e->getCode(), $e->getMessage());
        }

        try {
            $account = $this->userRepository->findBy(['login' => $login]);
        } catch (Exception $e) {
            $this->returnResponse($e->getCode(), $e->getMessage());
        }

        if ($account === null || !password_verify($password, $account->getPassword())) {
            return $this->returnResponse(self::HTTP_CODES['bad_request'], self::MESSAGES['login_error']);
        }

        session_start();
        setcookie('token', sha1(session_id()));
        if (is_null($tokenRepository->findBy(['value' => sha1(session_id())]))) {
            $tokenRepository->add(sha1(session_id()));
        }

        $_SESSION['login'] = $account->getLogin();
        $_SESSION['email'] = $account->getEmail();
        $_SESSION['password_hash'] = $account->getPassword();

        return $this->returnResponse(self::HTTP_CODES['ok']);
    }

    /**
     * @param array $data
     * @return bool|int
     */
    public function registrationAction(array $data): bool|int
    {
        $login = $data['login'] ?? null;
        $password = $data['password'] ?? null;
        $password_repeat = $data['password_repeat'] ?? null;
        $email = $data['email'] ?? null;

        try {
            Validator::registrationValidate($login, $password, $password_repeat, $email);
        } catch (Exception $e) {
            return $this->returnResponse($e->getCode(), $e->getMessage());
        }

        try {
            if ($this->userRepository->findBy(['login' => $login]) !== null) {
                $this->returnResponse(self::HTTP_CODES['bad_request'], self::MESSAGES['login_exists']);
            }

            if ($this->userRepository->findBy(['email' => $email]) !== null) {
                $this->returnResponse(self::HTTP_CODES['bad_request'], self::MESSAGES['email_exists']);
            }
        } catch (Exception $e) {
            $this->returnResponse($e->getCode(), $e->getMessage());
        }

        if ($password !== $password_repeat) {
            $this->returnResponse(self::HTTP_CODES['bad_request'], self::MESSAGES['different_passwords']);
        }

        (new UserRepository())->add($login, password_hash($password, PASSWORD_DEFAULT), $email);

        return $this->returnResponse(self::HTTP_CODES['ok'], self::MESSAGES['registration_successful']);
    }
}