<?php

namespace App\Controllers;

use App\Classes\Validator;
use Exception;
use App\Orm\Repositories\TokenRepository;
use App\Orm\Repositories\UserRepository;

class WelcomeController extends Controller
{
    private const MESSAGES = [
        'server_error' => 'Something went wrong, try to reload page or come back later.',
        'change_email_error' => 'User with this email is already exists.',
        'change_email_success' => 'Email changed successfully.',
        'change_password_success' => 'Password changed successfully.',
    ];

    public function getDataAction(): bool|int
    {
        if (empty($_SESSION['login']) || empty($_SESSION['email']) || empty($_SESSION['password_hash'])) {
            return $this->returnResponse(self::HTTP_CODES['internal_server_error'], self::MESSAGES['server_error']);
        }
        return $this->returnResponse(self::HTTP_CODES['ok'], json_encode([
            'login' => $_SESSION['login'],
            'email' => $_SESSION['email'],
        ]));
    }

    /**
     * @throws Exception
     */
    public function changeEmailAction($data)
    {
        $userRepository = new UserRepository();
        $currentEmail = $userRepository->findBy(['login' => $_SESSION['login'] ?? null])->getEmail();

        if ($currentEmail === null) {
            $this->returnResponse(self::HTTP_CODES['internal_server_error'], self::MESSAGES['server_error']);
        }

        $newEmail = $data['new_email'] ?? null;

        try {
            Validator::changeEmailValidate($currentEmail, $newEmail);
        } catch (Exception $e) {
            $this->returnResponse($e->getCode(), $e->getMessage());
        }

        if ($userRepository->findBy(['email' => $newEmail]) !== null) {
            $this->returnResponse(self::HTTP_CODES['bad_request'], self::MESSAGES['change_email_error']);
        }

        if (!($userRepository->updateBy(['email' => $newEmail], ['login' => $_SESSION['login']]))) {
            $this->returnResponse(self::HTTP_CODES['internal_server_error'], self::MESSAGES['server_error']);
        }
        $_SESSION['email'] = $newEmail;

        $this->returnResponse(self::HTTP_CODES['ok'], self::MESSAGES['change_email_success']);
    }

    public function changePasswordAction($data)
    {
        $currentPassword = $data['current_password'] ?? null;
        $newPassword = $data['new_password'] ?? null;
        $confirmNewPassword = $data['confirm_new_password'] ?? null;

        try {
            Validator::changePasswordValidate($currentPassword, $newPassword, $confirmNewPassword);
        } catch (Exception $e) {
            $this->returnResponse($e->getCode(), $e->getMessage());
        }

        if (!(new UserRepository())->updateBy(['password' => password_hash($newPassword, PASSWORD_DEFAULT)], ['login' => $_SESSION['login']])) {
            $this->returnResponse(self::HTTP_CODES['internal_server_error'], self::MESSAGES['server_error']);
        }
        $_SESSION['password_hash'] = password_hash($newPassword, PASSWORD_DEFAULT);

        $this->returnResponse(self::HTTP_CODES['ok'], self::MESSAGES['change_password_success']);
    }

    public function exitAction(): bool|int
    {
        if (empty($_COOKIE['token'])) {
            $this->returnResponse(self::HTTP_CODES['internal_server_error'], self::MESSAGES['server_error']);
        }
        $tokenRepository = new TokenRepository();
        if (!($tokenRepository->deleteBy(['value' => $_COOKIE['token']]))) {
            $this->returnResponse(self::HTTP_CODES['internal_server_error'], self::MESSAGES['server_error']);
        }
        setcookie('token', null, time() - 3600, '/');
        unset($_COOKIE['token']);
        session_destroy();
        return $this->returnResponse(self::HTTP_CODES['ok']);
    }
}