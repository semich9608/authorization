<?php

namespace App\Controllers;

abstract class Controller
{
    protected const HTTP_CODES = array(
        // Informational 1xx
        'continue' => 100,
        'switching_protocols' => 101,

        // Success 2xx
        'ok' => 200,
        'created' => 201,
        'accepted' => 202,
        'non-authoritative_information' => 203,
        'no_content' => 204,
        'reset_content' => 205,
        'partial_content' => 206,

        // Redirection 3xx
        'multiple_choices' => 300,
        'moved_permanently' => 301,
        'found' => 302,
        'see_other' => 303,
        'not_modified' => 304,
        'use_proxy' => 305,
        'temporary_redirect' => 307,

        // Client Error 4xx
        'bad_request' => 400,
        'unauthorized' => 401,
        'payment_required' => 402,
        'forbidden' => 403,
        'not_found' => 404,
        'method_not_allowed' => 405,
        'not_acceptable' => 406,
        'proxy_authentication_required' => 407,
        'request_timeout' => 408,
        'conflict' => 409,
        'gone' => 410,
        'length_required' => 411,
        'precondition_failed' => 412,
        'request_entity_too_large' => 413,
        'request-uri_too_long' => 414,
        'unsupported_media_type' => 415,
        'requested_range_not_satisfiable' => 416,
        'expectation_failed' => 417,

        // Server Error 5xx
        'internal_server_error' => 500,
        'not_implemented' => 501,
        'bad_gateway' => 502,
        'service_unavailable' => 503,
        'gateway_timeout' => 504,
        'http_version_not_supported' => 505,
        'bandwidth_limit_exceeded' => 509,
    );

    protected function returnResponse(int $code, string $error = null): bool|int
    {
        http_response_code($code);
        exit($error);
    }
}