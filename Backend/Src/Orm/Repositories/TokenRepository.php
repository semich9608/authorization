<?php

namespace App\Orm\Repositories;

use mysqli_result;
use App\Orm\Models\Token;

class TokenRepository extends AbstractRepository
{
    private const TABLE = 'tokens';
    private const INSERT_QUERY_TEMPLATE = 'INSERT INTO `work5`.`%s`(`value`) VALUE (%s)';

    public function __construct()
    {
        parent::__construct();
    }

    public function findBy(array $parameters): null|Token
    {
        $query = sprintf(
            self::SELECT_QUERY_TEMPLATE,
            self::TABLE,
            $this->prepareParameters($parameters)
        );
        $queryResult = $this->dbConnection->query($query)->fetch_assoc();

        if (!$queryResult)
            return null;

        return (new Token())
            ->setId($queryResult['id'] ?? null)
            ->setCreatedAt($queryResult['created_at'] ?? null)
            ->setValue($queryResult['value'] ?? null);

    }

    public function add(string $value): mysqli_result|bool
    {
        $query = sprintf(
            self::INSERT_QUERY_TEMPLATE,
            self::TABLE,
            $this->setQuotes($value)
        );
        return $this->dbConnection->query($query);
    }

    public function deleteBy(array $parameters): mysqli_result|bool
    {
        $query = sprintf(
            self::DELETE_QUERY_TEMPLATE,
            self::TABLE,
            $this->prepareParameters($parameters)
        );
        return $this->dbConnection->query($query);
    }
}