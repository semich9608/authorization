<?php

namespace App\Orm\Repositories;

interface RepositoryInterface
{
    const SELECT_QUERY_TEMPLATE = 'SELECT * FROM `work5`.`%s` WHERE %s';
    const UPDATE_QUERY_TEMPLATE = 'UPDATE %s SET %s WHERE %s';
    const DELETE_QUERY_TEMPLATE = 'DELETE FROM %s WHERE %s';

    public function __construct();

    public function findBy(array $parameters);
}