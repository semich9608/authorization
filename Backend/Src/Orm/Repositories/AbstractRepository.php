<?php

namespace App\Orm\Repositories;

use Exception;
use mysqli;
use App\Orm\DbConnectionWork5;

abstract class AbstractRepository implements RepositoryInterface
{
    private const TABLE = '';

    protected mysqli|bool|null $dbConnection;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->dbConnection = (new DbConnectionWork5())->getConnection();
    }

    public function findBy(array $parameters)
    {
        // TODO: Implement findBy() method.
    }

    protected function prepareParameters(array $params, bool $forUpdateSet = false): string
    {
        $parameters = '';
        $counter = 0;
        foreach ($params as $keyParam => $paramValue) {
            if ($counter === 0) {
                $parameters .= $forUpdateSet ? "" : "BINARY ";
                $parameters .= $keyParam . " = " . $this->setQuotes($paramValue);
                $counter++;
                continue;
            }
            $parameters .= $forUpdateSet ? "" : " AND BINARY ";
            $parameters .= $keyParam . " = " . $this->setQuotes($paramValue);
        }
        return $parameters;
    }

    protected function setQuotes(string $string): string
    {
        return "'" . $string . "'";
    }
}