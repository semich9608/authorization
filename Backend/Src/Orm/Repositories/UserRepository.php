<?php

namespace App\Orm\Repositories;

use mysqli_result;
use App\Orm\Models\User;

class UserRepository extends AbstractRepository
{
    private const TABLE = 'users';
    private const INSERT_QUERY_TEMPLATE = 'INSERT INTO `work5`.`%s`(`login`, `password`, `email`) VALUE (%s, %s, %s)';

    public function __construct()
    {
        parent::__construct();
    }

    public function findBy(array $parameters): null|User
    {
        $query = sprintf(
            self::SELECT_QUERY_TEMPLATE,
            self::TABLE,
            $this->prepareParameters($parameters)
        );
        $queryResult = $this->dbConnection->query($query)->fetch_assoc();

        if (!$queryResult)
            return null;

        return (new User())
            ->setId($queryResult['id'] ?? null)
            ->setLogin($queryResult['login'] ?? null)
            ->setPassword($queryResult['password'] ?? null)
            ->setEmail($queryResult['email'] ?? null);
    }

    public function add($login, $password, $email): mysqli_result|bool
    {
        $query = sprintf(
            self::INSERT_QUERY_TEMPLATE,
            self::TABLE,
            $this->setQuotes($login),
            $this->setQuotes($password),
            $this->setQuotes($email)
        );
        return $this->dbConnection->query($query);
    }

    public function updateBy(array $parametersToUpdate, array $parametersWhereUpdate): mysqli_result|bool
    {
        $query = sprintf(
            self::UPDATE_QUERY_TEMPLATE,
            self::TABLE,
            $this->prepareParameters($parametersToUpdate, true),
            $this->prepareParameters($parametersWhereUpdate)
        );
        return $this->dbConnection->query($query);
    }
}