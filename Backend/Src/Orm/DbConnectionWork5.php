<?php

namespace App\Orm;

use Exception;
use mysqli;

class DbConnectionWork5
{
    public false|null|mysqli $connection;

    public function __construct()
    {
        $this->connection = $this->init();
    }

    private function init(): bool|null|mysqli
    {
        return (mysqli_connect(
                $_ENV['ENV_WORK5_HOST'],
                $_ENV['ENV_WORK5_USERNAME'],
                $_ENV['ENV_WORK5_PASSWORD'],
                $_ENV['ENV_WORK5_DATABASE'],
                $_ENV['ENV_WORK5_PORT']
            )) ?? null;
    }

    public function getConnection(): bool|null|mysqli
    {
        if (!$this->connection instanceof mysqli)
            throw new Exception('Database connection error.', 500);
        return $this->connection;
    }
}