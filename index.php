<?php

namespace App;

use App\Classes\Router;
use Dotenv\Dotenv;
use App\Orm\Repositories\TokenRepository;

require_once __DIR__ . '/vendor/autoload.php';
Dotenv::createImmutable(__DIR__)->load();
session_start();

const METHOD_GET = 'GET';
const METHOD_POST = 'POST';

$requestMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];
$startUris = ['/auth', '/registration', '/', ''];

$router = new Router();

if ($requestMethod === METHOD_GET) {
    //Проверка на существование адреса
    if (!in_array($uri, $router->getGetRoutes())) {
        $router->methodGet($uri);
    }

    //Проверка на существование токена атворизации
    if (empty($_COOKIE['token'])) {
        !in_array($uri, $startUris) ? $router->methodGet('/auth') : $router->methodGet($uri);
    }
    //Действия, если токен авторизации есть
    $token = (new TokenRepository())->findBy(['value' => $_COOKIE['token'] ?? '']);
    if (!is_null($token)) {
        in_array($uri, $startUris) ? $router->methodGet('/welcome') : $router->methodGet($uri);
    } else {
        $router->methodGet('/auth');
    }
}

if ($requestMethod === METHOD_POST) {
    $router->methodPost($uri, $_POST);
}
