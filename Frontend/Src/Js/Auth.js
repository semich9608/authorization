$(document).ready(function () {
    $("#submit_button_authorization").click(function (event) {
        event.preventDefault();
        let login = $("#login").val(),
            password = $("#password").val(),
            error_block = $("#error");

        error_block.hide();

        if (login.trim() === '' || password.trim() === '') {
            setError(error_block, 'Login and password can`t be a null.');
            return;
        }

        $.ajax({
            type: 'POST',
            data: {
                login: login,
                password: password
            },
            url: '/authorization',
            success: function () {
                window.location.href = '/welcome';
            },
            error: function (error) {
                setError(error_block, error.responseText);
            }
        })
    })

    function setError(error, message) {
        error.text(message);
        error.show();
    }
})