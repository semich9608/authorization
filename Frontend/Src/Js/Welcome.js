$(document).ready(function () {
    var login = $("#login"),
        email = $("#email"),
        error_block = $("#error"),
        success_block = $("#success"),
        change_email_section = $("#change_email_section"),
        change_password_section = $("#change_password_section");

    $.ajax({
        type: 'POST',
        url: '/welcome/get_data',
        success: function (data, textStatus, response) {
            if (response.status === 200) {
                data = JSON.parse(data);
                login.text(data['login']);
                email.text(data['email']);
            }
        },
        error: function (error) {
            setError(error_block, error.responseText);
        }
    })

    $("#show_change_email_section").click(function () {
        $("#current_password").val("");
        $("#new_password").val("");
        $("#confirm_new_password").val("");
        error_block.hide();
        success_block.hide();
        if (change_password_section.css('display') === 'block') {
            change_password_section.hide();
        }
        change_email_section.show();
    })

    $("#show_change_password_section").click(function () {
        $("#new_email").val("");
        error_block.hide();
        success_block.hide();
        if (change_email_section.css('display') === 'block') {
            change_email_section.hide();
        }
        change_password_section.show();
    })

    $("#change_email").click(function () {
        let new_email = $("#new_email").val(),
            regex_email = /^[A-Za-z0-9]{1,128}@[A-Za-z0-9]{1,212}\.[a-z]{1,3}$/;

        error_block.hide();
        success_block.hide();

        if (new_email.trim() === '') {
            setError(error_block, 'Email can`t be a null.');
            return;
        }

        if (!new_email.match(regex_email)) {
            setError(error_block, 'Email must be lock like email address.');
            return;
        }

        $.ajax({
            type: 'POST',
            data: {
                new_email: new_email
            },
            url: '/welcome/change_email',
            success: function (data, textStatus, response) {
                if (response.status === 200) {
                    email.text(new_email);
                    setSuccess(success_block, data)
                }
            },
            error: function (error) {
                setError(error_block, error.responseText);
            }
        })
    })

    $("#change_password").click(function () {
        let current_password = $("#current_password").val(),
            new_password = $("#new_password").val(),
            confirm_new_password = $("#confirm_new_password").val(),
            regex_password = /^[A-Za-z0-9]{1,50}$/;

        error_block.hide();
        success_block.hide();

        if (current_password.trim() === '' || new_password.trim() === '' || confirm_new_password.trim() === '') {
            setError(error_block, 'Current password, new password and new repeating password can`t be a null.');
            return;
        }

        if (!current_password.match(regex_password) || !new_password.match(regex_password) || !confirm_new_password.match(regex_password)) {
            setError(error_block, 'Current password, new password and repeating password be must be more than 0 characters, but no more than 50 characters and consist of Latin letters or numbers.');
            return;
        }

        if (new_password !== confirm_new_password) {
            setError(error_block, 'New password and repeating new password must be the same.');
            return;
        }

        if (current_password === new_password) {
            setError(error_block, 'Current and new passwords must be a different.');
            return;
        }

        $.ajax({
            type: 'POST',
            data: {
                current_password: current_password,
                new_password: new_password,
                confirm_new_password: confirm_new_password
            },
            url: '/welcome/change_password',
            success: function (data, textStatus, response) {
                if (response.status === 200) {
                    setSuccess(success_block, data)
                }
            },
            error: function (error) {
                setError(error_block, error.responseText);
            }
        })
    })

    $("#exit").click(function () {
        $.ajax({
            type: 'POST',
            url: '/welcome/exit',
            success: function (data, textStatus, response) {
                if (response.status === 200) {
                    window.location.href = '/auth';
                }
            },
            error: function (error) {
                setError(error_block, error.responseText);
            }
        })
    })

    function setError(error, message) {
        error.text(message);
        error.show();
    }

    function setSuccess(success, message) {
        success.text(message);
        success.show();
    }
})