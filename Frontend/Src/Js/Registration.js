$(document).ready(function () {
    $("#submit_button_registration").click(function (event) {
        event.preventDefault();
        let login = $("#login").val(),
            email = $("#email").val(),
            password = $("#password").val(),
            password_repeat = $("#password_repeat").val(),
            error_block = $("#error"),
            success_block = $("#success"),
            regex_login = /^[A-Za-z0-9]{1,100}$/,
            regex_password = /^[A-Za-z0-9]{1,50}$/,
            regex_email = /^[A-Za-z0-9]{1,128}@[A-Za-z0-9]{1,212}\.[a-z]{1,3}$/;

        error_block.hide();
        success_block.hide();

        if (login.trim() === '' || email.trim() === '' || password.trim() === '' || password_repeat.trim() === '') {
            setError(error_block, 'Login, email, password and repeating password can`t be a null.');
            return;
        }

        if (!login.match(regex_login)) {
            setError(error_block, 'Login must be more than 0 characters, but no more than 100 characters and consist of Latin letters or numbers.');
            return;
        }

        if (!email.match(regex_email)) {
            setError(error_block, 'Email must be lock like email address.');
            return;
        }

        if (password !== password_repeat) {
            setError(error_block, 'Passwords must be the same.');
            return;
        }

        if (!password.match(regex_password)) {
            setError(error_block, 'Password and repeating password be must be more than 0 characters, but no more than 50 characters and consist of Latin letters or numbers.')
            return;
        }

        $.ajax({
            type: 'POST',
            data: {
                login: login,
                email: email,
                password: password,
                password_repeat: password_repeat
            },
            url: '/registration',
            success: function (data, text_status, response) {
                if (response.status === 200) {
                    setSuccess(success_block, data);
                    setTimeout(() => window.location.replace('/auth'), 1000)
                    //window.location.href = '/auth';
                }
            },
            error: function (error) {
                setError(error_block, error.responseText);
            }
        })
    })

    function setError(error, message) {
        error.text(message);
        error.show();
    }

    function setSuccess(success, message) {
        success.text(message);
        success.show();
    }
})